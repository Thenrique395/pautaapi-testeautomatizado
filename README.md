# Desafio Pauta -  Automação da Pauta API

## Sobre o projeto

Projeto que faz os testes automatizado da PautaAPI.

### Tecnologias utilizadas

- Java 8
- Maven
- Rest Assured
- Junit

### Ferramentas utilizadas

- Postman 
- Git 


### Teste

Para testar a aplicação, seguir o seguinte caminho:

```
src > test > java > Steps {Escolher o cenario de teste}

```

### Observações do projeto de teste

Para testar os end-point que possui parametros,foi criado um pacote chamado "Util" onde criei classes, que nestas classes
possui constantes contendo as urls e ids dos parametros.

```
src > test > java > Util {Escolher a classe de parametrização}

```

```
### Contribuição

Alterações e novas melhorias fique a vontade para criar novos pull request.

### Contatos: 


Telefone : (81)99525-7823

Linkedin : [Tiago Henrique ]("https://www.linkedin.com/in/tiago-henrique-dos-santos-083979101/")

E-mail : "Thenrique395@gmail.com"
