package Steps;

import Util.Url_Associado;
import io.restassured.http.ContentType;
import org.json.JSONObject;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StepsDefinition_Associado {


    @Test
    public void CT01_cadastrarAssociado() {

        JSONObject requestParams = new JSONObject();
        requestParams.put("cpf", "08096516027");
        requestParams.put("nome", "Maria Madalena");
        requestParams.put("email", "maria@email.com");
        requestParams.put("fk_pauta_Associado", "3");

        given().contentType(ContentType.JSON)
                .body(requestParams.toString()).when()
                .post(Url_Associado.BASE_URL_ASSOCIADO ).then().statusCode(201).log().body().extract().response();
    }

     @Test
    public void CT02_cadastrarAssociadoExistente() {

        JSONObject requestParams = new JSONObject();
        requestParams.put("cpf", "08096516027");
        requestParams.put("nome", "Maria Madalena");
        requestParams.put("email", "maria@email.com");
        requestParams.put("fk_pauta_Associado", "3");

        Response res =  given().contentType(ContentType.JSON)
                .body(requestParams.toString()).when()
                .post(Url_Associado.BASE_URL_ASSOCIADO).then().statusCode(500).extract().response();

          System.out.println(res.jsonPath().getString("message"));
    }

    @Test
    public void CT03_atualizarAssociado() {

        JSONObject requestParams = new JSONObject();
        requestParams.put("idAssociado", "4");
        requestParams.put("cpf", "07845121401");
        requestParams.put("nome", "Maria Antonia");
        requestParams.put("email", "Mariaantonia@email.com");
        requestParams.put("fk_pauta_Associado", "3");

        given().contentType(ContentType.JSON)
                .body(requestParams.toString()).when()
                .put(Url_Associado.BASE_URL_ASSOCIADO ).then().statusCode(201).log().body().extract().response();
    }

    @Test
    public void CT04_deletarAssociado() {

        given().contentType(ContentType.JSON).
                when().
                delete(Url_Associado.BASE_URL_ASSOCIADO_CONSULTAR_POR_ID + Url_Associado.DELETAR_ID).then().statusCode(204).log().body().extract().response();
    }


   @Test
    public void CT05_consultarAssociadoPorId(){
        given().contentType(ContentType.JSON).
                when().
                get(Url_Associado.BASE_URL_ASSOCIADO_CONSULTAR_POR_ID + Url_Associado.CONSULTAR_ASSOCIADO_POR_ID ).then().statusCode(200).log().body().extract().response();
    }


      @Test
    public void CT06_listarTodosAssocidos() {

                given().contentType(ContentType.JSON).
                        when().
                        get(Url_Associado.BASE_URL_ASSOCIADO).then().statusCode(200).log().body().extract().response();

    }

}


