package Steps;

import Util.Url_Pauta;
import io.restassured.http.ContentType;
import org.json.JSONObject;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StepsDefinition_Pauta {

    @Test
    public void CT01_cadastrarPauta() {

        JSONObject requestParams = new JSONObject();
        requestParams.put("tituloPauta", "Pauta Teste");
        requestParams.put("autor", "Maria");
        requestParams.put("detalhePauta", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's " +
                "standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.");
         Response res =
        given().contentType(ContentType.JSON)
                .body(requestParams.toString()).when()
                .post(Url_Pauta.BASE_URL_PAUTA).then().statusCode(201).log().body().extract().response();
    }

    @Test
    public void CT02_atualizarPauta() {

        JSONObject requestParams = new JSONObject();
        requestParams.put("idpauta", "1");
        requestParams.put("tituloPauta", "Pauta 04");
        requestParams.put("autor", "Henrique");
        requestParams.put("detalhePauta", "Lorem Ipsum is simply dummy text of the printing and typesetting industry.");
        given().contentType(ContentType.JSON)
                .body(requestParams.toString()).when()
                .put(Url_Pauta.BASE_URL_PAUTA).then().statusCode(200).log().body().extract().response();
    }

    @Test
    public void CT03_deletarPauta() {

        given().contentType(ContentType.JSON).
                when().
                delete(Url_Pauta.BASE_URL_PAUTA_COM_PARAMETRO + Url_Pauta.ID_DELETAR_PAUTA).then().statusCode(200).log().body().extract().response();
    }

    @Test
    public void CT04_deletarPautaNaoCadastrada() {
        Response response =
                given().contentType(ContentType.JSON).
                        when().
                        delete(Url_Pauta.BASE_URL_PAUTA_COM_PARAMETRO + Url_Pauta.ID_DELETAR_PAUTA_NAO_CADASTRADA).then().statusCode(500).extract().response();
        System.out.println(response.jsonPath().getString("message"));
    }


    @Test
    public void CT05_consultarPautaPorId(){
        given().contentType(ContentType.JSON).
                when().
                get(Url_Pauta.BASE_URL_PAUTA_COM_PARAMETRO + Url_Pauta.ID_CONSULTAR_PAUTA ).then().statusCode(200).log().body().extract().response();
    }

    @Test
    public void CT06_consultarPautaPorIdQueNaoExiste(){
        Response response =
        given().contentType(ContentType.JSON).
                when().
                get(Url_Pauta.BASE_URL_PAUTA_COM_PARAMETRO + Url_Pauta.ID_CONSULTAR_PAUTA_NAO_EXISTE ).then().statusCode(500).extract().response();
        System.out.println(response.jsonPath().getString("message"));
    }

    @Test
    public void CT07_listarTodosPauta() {
        given().contentType(ContentType.JSON).
                when().
                get(Url_Pauta.BASE_URL_PAUTA).then().statusCode(200).log().body().extract().response();
    }


}
