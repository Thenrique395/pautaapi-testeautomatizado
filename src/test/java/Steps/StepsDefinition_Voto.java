package Steps;

import Util.Url_Voto;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import static io.restassured.RestAssured.given;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StepsDefinition_Voto {

    @Test
    public void CT01_abrirSessao(){
        given().contentType(ContentType.JSON).
                when().
                get(Url_Voto.BASE_URL_ABRIR_SESSAO+ Url_Voto.PARAMETRO_ID + Url_Voto.TEMPO).then().statusCode(200).log().body().extract().response();
    }

    @Test
    public void CT01_cadastrarVoto() {

        JSONObject requestParams = new JSONObject();
        requestParams.put("cpfAssociado", "08096516027");
        requestParams.put("voto", "SIM");
        requestParams.put("pauta", "3");
                given().contentType(ContentType.JSON)
                .body(requestParams.toString()).when()
                .post(Url_Voto.BASE_URL_VOTO).then().statusCode(201).log().body().extract().response();
    }

    @Test
    public void CT02_cadastrarVotoSessaoFechada() {

        JSONObject requestParams = new JSONObject();
        requestParams.put("cpfAssociado", "07719831071");
        requestParams.put("voto", "SIM");
        requestParams.put("pauta", "2");
        Response response =
        given().contentType(ContentType.JSON).body(requestParams.toString()).when()
                .post(Url_Voto.BASE_URL_VOTO).then().statusCode(500).extract().response();
              System.out.println(response.jsonPath().getString("message"));
    }

}
