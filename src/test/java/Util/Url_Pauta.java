package Util;

public class Url_Pauta {

    public static final String BASE_URL_PAUTA = "http://localhost:8080/api/Pauta";

    public static final String BASE_URL_PAUTA_COM_PARAMETRO = "http://localhost:8080/api/Pauta/";


    public static final String ID_CONSULTAR_PAUTA = "2";
    public static final String ID_CONSULTAR_PAUTA_NAO_EXISTE = "100";

    public static final String ID_DELETAR_PAUTA = "2";
    public static final String ID_DELETAR_PAUTA_NAO_CADASTRADA = "3";
}
